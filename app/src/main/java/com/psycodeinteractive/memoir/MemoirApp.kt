package com.psycodeinteractive.memoir

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.psycodeinteractive.memoir.di.DI

class MemoirApp : Application() {

    override fun attachBaseContext(base: Context?) {
        MultiDex.install(this)
        super.attachBaseContext(base)
    }

    override fun onCreate() {
        super.onCreate()
        initLibs()
    }

    private fun initLibs() {
        DI.init(this)
    }
}