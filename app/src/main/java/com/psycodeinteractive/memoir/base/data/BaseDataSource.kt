package com.psycodeinteractive.memoir.base.data

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flowOf

abstract class BaseDataSource<Item: Any> {

    open fun add(data: Item): Flow<Item> = empty(data)

    open fun update(data: Item): Flow<Item> = empty(data)

    open fun updateMultiple(data: List<Item>): Flow<List<Item>> = emptyList(data)

    open fun remove(dataSpecs: Specification<Item>): Flow<Unit> = emptyFlow()

    open fun querySingle(dataSpecs: Specification<Item>): Flow<Item> = emptyFlow()

    open fun query(dataSpecs: Specification<Item>): Flow<List<Item>> = emptyList()

    protected fun empty(item: Item) = flowOf(item)
    protected fun emptyList(item: List<Item>? = null) = flowOf(item ?: listOf())
}