package com.psycodeinteractive.memoir.base.data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

open class BaseRepository<Item: BaseModel>(
    protected open val localDataSource: BaseDataSource<Item>,
    protected open val remoteDataSource: BaseDataSource<Item>? = null
): BaseDataSource<Item>() {

    override fun add(data: Item): Flow<Item> {
        return localDataSource.add(data)
            .run { applySchedulers(this) }
    }

    override fun update(data: Item): Flow<Item> {
        return localDataSource.update(data)
            .run { applySchedulers(this) }
    }

    override fun updateMultiple(data: List<Item>): Flow<List<Item>> {
        return localDataSource.updateMultiple(data)
            .run { applySchedulers(this) }
    }

    override fun remove(dataSpecs: Specification<Item>): Flow<Unit> {
        return localDataSource.remove(dataSpecs)
            .run { applySchedulers(this) }
    }

    override fun querySingle(dataSpecs: Specification<Item>): Flow<Item> {
        return localDataSource.querySingle(dataSpecs)
            .run { applySchedulers(this) }
    }

    override fun query(dataSpecs: Specification<Item>): Flow<List<Item>> {
        return localDataSource.query(dataSpecs)
            .run { applySchedulers(this) }
    }

    @ExperimentalCoroutinesApi
    protected fun <T> applySchedulers(theFlow: Flow<T>) = theFlow.flowOn(Dispatchers.Default)
}
