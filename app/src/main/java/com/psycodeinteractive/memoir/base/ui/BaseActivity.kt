package com.psycodeinteractive.memoir.base.ui

import android.os.Bundle
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.setContent
import androidx.fragment.app.FragmentActivity
import com.psycodeinteractive.memoir.util.ExternalResultsManager
import com.psycodeinteractive.memoir.util.ui.MemoirTheme

abstract class BaseActivity: FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ExternalResultsManager.init(this)

        setContent {
            MemoirTheme {
                ViewRoot()
            }
        }
    }

    @Composable
    abstract fun ViewRoot()
}