package com.psycodeinteractive.memoir.base.ui

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

abstract class BaseViewModel<BaseState: CommonState, BaseEvent: Event>: ViewModel() {

    val state by lazy { MutableStateFlow(StateWrapper(getInitialState())) }

    val events by lazy { MutableSharedFlow<BaseEvent>() }

    val StateFlow<StateWrapper<BaseState>>.current
        get() = this.value.state

    abstract fun getInitialState(): BaseState

    fun StateFlow<StateWrapper<BaseState>>.mutate(mutation: BaseState.() -> Unit) {
        val currentState = state.value.state
        mutation(currentState)
        val newStateWrapped = StateWrapper(currentState)
        state.value = newStateWrapped
    }

    class StateWrapper<BaseState: CommonState>(val state: BaseState)

    protected suspend fun BaseEvent.dispatch() = events.emit(this)
}

