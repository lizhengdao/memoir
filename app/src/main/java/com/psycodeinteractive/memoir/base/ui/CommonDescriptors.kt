package com.psycodeinteractive.memoir.base.ui

abstract class CommonState(
    var isLoading: Boolean = false,
    var isRefreshing: Boolean = false,
    var hasChangeOccurred: Boolean = false
)