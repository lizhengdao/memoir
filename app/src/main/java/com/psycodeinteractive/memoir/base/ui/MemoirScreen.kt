package com.psycodeinteractive.memoir.base.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.ViewModelStoreOwnerAmbient
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelStoreOwner
import org.koin.androidx.viewmodel.koin.getViewModel
import org.koin.core.context.KoinContextHandler
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier

@OptIn(ExperimentalAnimationApi::class)
@Composable
inline fun <reified VM : BaseViewModel<out CommonState, out Event>> MemoirScreen(
    crossinline children: @Composable (viewModel: VM) -> Unit
) {
    val viewModel: VM = getViewModel()

    val animSpec = tween<Float>(screenTransitionDurationMs)
    AnimatedVisibility(
        enter = fadeIn(animSpec = animSpec),
        exit = fadeOut(animSpec = animSpec),
        visible = true,
        initiallyVisible = false
    ) {
        children(viewModel)
    }
}

@Composable
inline fun <reified T : ViewModel> getViewModel(
    owner: ViewModelStoreOwner? = null,
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null,
): T {
    val o = owner ?: ViewModelStoreOwnerAmbient.current
    return remember {
        KoinContextHandler.get().getViewModel(
            owner = o,
            qualifier = qualifier,
            parameters = parameters
        )
    }
}

const val screenTransitionDurationMs = 600