package com.psycodeinteractive.core.ui.route

interface Route {
    /* TODO: Make sure that this still works after updating Navigation library
        or check if getting current screen has been implemented
        */
    val id
            get() = this.hashCode() + 0x00010000

    val args: String?
}
