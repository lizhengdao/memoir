package com.psycodeinteractive.memoir.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.psycodeinteractive.memoir.base.data.BaseModel
import com.psycodeinteractive.memoir.data.model.Entry.Companion.tableName
import kotlinx.datetime.LocalDateTime

@Entity(tableName = tableName)
data class Entry(
    val timeStamp: LocalDateTime,
    val text: String? = null,
    val pictureUrl: String? = null,
    val voiceRecUrl: String? = null,
    val videoUrl: String? = null
): BaseModel {
    @PrimaryKey(autoGenerate = true) var id: Long = 0

    companion object {
        const val tableName = "entry_table"
    }
}