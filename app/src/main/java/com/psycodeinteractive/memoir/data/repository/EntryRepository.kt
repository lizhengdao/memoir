package com.psycodeinteractive.memoir.data.repository

import com.psycodeinteractive.memoir.base.data.BaseRepository
import com.psycodeinteractive.memoir.data.model.Entry
import com.psycodeinteractive.memoir.data.source.EntryLocalDataSource
import com.psycodeinteractive.memoir.data.source.local.LocalDataSource

class EntryRepository(
    override val localDataSource: EntryLocalDataSource
): BaseRepository<Entry>(localDataSource)