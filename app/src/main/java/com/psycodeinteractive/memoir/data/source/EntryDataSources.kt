package com.psycodeinteractive.memoir.data.source

import com.psycodeinteractive.memoir.base.data.Specification
import com.psycodeinteractive.memoir.data.model.Entry
import com.psycodeinteractive.memoir.data.source.local.LocalDataSource
import com.psycodeinteractive.memoir.data.source.local.dao.EntryDao
import com.psycodeinteractive.memoir.data.specification.entry.EntriesByDaySpecs
import com.psycodeinteractive.memoir.data.specification.entry.EntryByIdSpecs
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flow

class EntryLocalDataSource constructor(
    private val entryDao: EntryDao
): LocalDataSource<Entry>(entryDao) {

    override fun add(data: Entry) = flow<Entry> { entryDao.insert(data) }

    override fun querySingle(dataSpecs: Specification<Entry>) = when (dataSpecs) {
        is EntryByIdSpecs -> entryDao.getEntryForId(dataSpecs.id)
        else -> emptyFlow()
    }

    override fun query(dataSpecs: Specification<Entry>) = when (dataSpecs) {
        is EntriesByDaySpecs -> entryDao.getEntriesForDay(dataSpecs.date)
        else -> emptyFlow()
    }

    override fun remove(dataSpecs: Specification<Entry>) = when (dataSpecs) {
        is EntryByIdSpecs -> flow<Unit> { entryDao.deleteEntryForId(dataSpecs.id) }
        else -> emptyFlow()
    }

}
