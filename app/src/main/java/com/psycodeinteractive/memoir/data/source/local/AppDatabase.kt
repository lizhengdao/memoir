package com.psycodeinteractive.memoir.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.psycodeinteractive.memoir.data.model.Entry
import com.psycodeinteractive.memoir.data.source.local.converter.Converters
import com.psycodeinteractive.memoir.data.source.local.dao.EntryDao

@Database(entities = [Entry::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun entryDao(): EntryDao
}