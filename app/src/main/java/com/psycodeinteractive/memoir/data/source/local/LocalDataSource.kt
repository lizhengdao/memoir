package com.psycodeinteractive.memoir.data.source.local

import com.psycodeinteractive.memoir.base.data.BaseDao
import com.psycodeinteractive.memoir.base.data.BaseDataSource
import com.psycodeinteractive.memoir.base.data.BaseModel


abstract class LocalDataSource<Model: BaseModel> constructor(
    protected val dao: BaseDao
): BaseDataSource<Model>()
