package com.psycodeinteractive.memoir.data.source.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.psycodeinteractive.memoir.base.data.BaseDao
import com.psycodeinteractive.memoir.data.model.Entry
import kotlinx.coroutines.flow.Flow
import kotlinx.datetime.LocalDate

@Dao
interface EntryDao: BaseDao {

    @Query("SELECT * FROM entry_table WHERE date(timeStamp) = date(:day) ORDER BY datetime(timeStamp) DESC")
    fun getEntriesForDay(day: LocalDate): Flow<List<Entry>>

    @Query("SELECT * FROM ${Entry.tableName} WHERE id LIKE :id")
    fun getEntryForId(id: Long): Flow<Entry>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entry: Entry)

    @Query("DELETE FROM entry_table WHERE id = :entryId")
    fun deleteEntryForId(entryId: Long)
}