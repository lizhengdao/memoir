package com.psycodeinteractive.memoir.data.specification.entry

import com.psycodeinteractive.memoir.base.data.Specification
import com.psycodeinteractive.memoir.data.model.Entry
import kotlinx.datetime.LocalDate

data class EntriesByDaySpecs(
    val date: LocalDate
): Specification<Entry>()
