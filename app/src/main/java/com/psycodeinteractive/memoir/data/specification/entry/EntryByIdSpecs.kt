package com.psycodeinteractive.memoir.data.specification.entry

import com.psycodeinteractive.memoir.base.data.Specification
import com.psycodeinteractive.memoir.data.model.Entry

data class EntryByIdSpecs(
    val id: Long
): Specification<Entry>()
