package com.psycodeinteractive.memoir.di

import android.app.Application
import com.psycodeinteractive.memoir.di.module.*
import com.psycodeinteractive.memoir.di.module.localDataSourceModule
import com.psycodeinteractive.memoir.di.module.remoteDataSourceModule
import com.psycodeinteractive.memoir.di.module.repoModule
import com.psycodeinteractive.memoir.di.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module

internal enum class DIModule(var module: Module) {
    Repo(repoModule),
    RemoteDataSource(remoteDataSourceModule),
    LocalDataSource(localDataSourceModule),
    DatabaseModule(databaseModule),
    ViewModel(viewModelModule)
}

object DI {
    fun init(application: Application) = startKoin {
        androidContext(application)
        modules(DIModule.values().map { it.module })
    }
}