package com.psycodeinteractive.memoir.di.module

import androidx.room.Room
import com.psycodeinteractive.memoir.data.repository.EntryRepository
import com.psycodeinteractive.memoir.data.source.EntryLocalDataSource
import com.psycodeinteractive.memoir.data.source.local.AppDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.Module
import org.koin.dsl.module

internal val repoModule: Module = module {
    single { EntryRepository(get()) }
}

internal val remoteDataSourceModule: Module = module {
}

internal val localDataSourceModule: Module = module {
    single { EntryLocalDataSource(get()) }
}

val databaseModule: Module = module {
    single {
        Room.databaseBuilder(
            androidApplication(),
            AppDatabase::class.java,
            "app_database"
        ).build()
    }

    fun provideDatabase(database: AppDatabase) = database

    single { provideDatabase(get()).entryDao() }
}