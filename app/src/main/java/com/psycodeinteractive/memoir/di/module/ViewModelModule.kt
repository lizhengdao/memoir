package com.psycodeinteractive.memoir.di.module

import com.psycodeinteractive.memoir.ui.feature.add.AddViewModel
import com.psycodeinteractive.memoir.ui.feature.details.DetailsViewModel
import com.psycodeinteractive.memoir.ui.feature.list.ListViewModel
import com.psycodeinteractive.memoir.ui.feature.splash.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

internal val viewModelModule: Module = module {
    viewModel { ListViewModel(get()) }
    viewModel { AddViewModel(get()) }
    viewModel { DetailsViewModel(get()) }
    viewModel { SplashViewModel() }
}