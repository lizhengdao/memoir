package com.psycodeinteractive.memoir.ui.feature.add

import com.psycodeinteractive.memoir.base.ui.CommonState
import com.psycodeinteractive.memoir.base.ui.Event
import com.psycodeinteractive.memoir.data.model.Entry

data class AddState(
    var textNote: String? = null,
    var soundUri: String? = null,
    var videoUri: String? = null,
    var pictureUri: String? = null
): CommonState()

enum class AddEvent: Event {
    EntrySaved
}
