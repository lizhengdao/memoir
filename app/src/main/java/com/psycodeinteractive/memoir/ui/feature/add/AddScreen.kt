package com.psycodeinteractive.memoir.ui.feature.add

import android.Manifest
import android.net.Uri
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ContextAmbient
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.fondesa.kpermissions.coroutines.sendSuspend
import com.fondesa.kpermissions.extension.permissionsBuilder
import com.fondesa.kpermissions.isGranted
import com.psycodeinteractive.memoir.R
import com.psycodeinteractive.memoir.base.ui.BaseActivity
import com.psycodeinteractive.memoir.base.ui.MemoirScreen
import com.psycodeinteractive.memoir.ui.widget.BackNavigationType
import com.psycodeinteractive.memoir.ui.widget.MemoirTextField
import com.psycodeinteractive.memoir.ui.widget.MemoirTopBar
import com.psycodeinteractive.memoir.ui.widget.ToolbarResources
import com.psycodeinteractive.memoir.util.ExternalResultsManager
import com.psycodeinteractive.memoir.util.RecordSound
import com.psycodeinteractive.memoir.util.TakePicture
import com.psycodeinteractive.memoir.util.TakeVideo
import com.psycodeinteractive.memoir.util.date.formatDate
import com.psycodeinteractive.memoir.util.date.getCurrentDateTime
import com.psycodeinteractive.memoir.util.media.MediaManager
import com.psycodeinteractive.memoir.util.ui.collectEventsAsComposeState
import com.psycodeinteractive.memoir.util.ui.themeTypography
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
fun AddScreen(
    exitScreen: () -> Unit,
) {
    MemoirScreen<AddViewModel> { viewModel ->
        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            MemoirTopBar(
                resources = provideTopBarResources(),
                onActionClick = { viewModel.saveEntry() }
            ) {
                exitScreen()
            }
            Text(
                modifier = Modifier.fillMaxWidth().padding(start = screenPadding),
                text = getCurrentDateTime().date.formatDate(),
                style = themeTypography.h6
            )
            DetailsItems(
                viewModel = viewModel
            )
        }
        when (viewModel.collectEventsAsComposeState()) {
            AddEvent.EntrySaved -> {
                ShowEntrySaved()
                exitScreen()
            }
        }
    }
}

@Composable
private fun ShowEntrySaved() {
    Toast.makeText(
        ContextAmbient.current,
        stringResource(id = R.string.entry_saved),
        Toast.LENGTH_LONG
    ).show()
}

@Composable
private fun DetailsItems(
    viewModel: AddViewModel
) {
    val result = ExternalResultsManager.uriExternalResults.collectAsState(null).value
    var recordSoundResult by remember { mutableStateOf( null as Uri? ) }
    var takePictureResult by remember { mutableStateOf( null as Uri? ) }
    var takeVideoResult by remember { mutableStateOf( null as Uri? ) }

    when (result?.first) {
        is RecordSound -> {
            recordSoundResult = result.second
            result.second.let { viewModel.onVoiceRecorded(it.toString()) }
        }
        is TakePicture -> {
            takePictureResult = result.second
            result.second.let { viewModel.onPictureTaken(it.toString()) }
        }
        is TakeVideo -> {
            takeVideoResult = result.second
            result.second.let { viewModel.onVideoTaken(it.toString()) }
        }
    }

    Column(
        modifier = Modifier.fillMaxSize()
            .padding(screenPadding)
    ) {
        MemoirTextField(
            modifier = commonModifier,
            placeholder = {
                Text(
                    text = stringResource(id = R.string.enter_text_note)
                )
            }
        ) {
            viewModel.onTextNoteChanged(it.text)
        }
        Item(
            mediaRequest = MediaManager.MediaRequest.Sound,
            result = recordSoundResult
        )
        Item(
            mediaRequest = MediaManager.MediaRequest.Photo,
            result = takePictureResult
        )
        Item(
            mediaRequest = MediaManager.MediaRequest.Video,
            result = takeVideoResult
        )
    }
}

@Composable
private fun Item(
    mediaRequest: MediaManager.MediaRequest,
    result: Uri? = null
) {
    val coroutineScope = rememberCoroutineScope()
    val activity = ContextAmbient.current as BaseActivity
    Text(
        modifier = commonModifier.clickable(enabled = result == null) {
            coroutineScope.request(activity, mediaRequest)
        },
        text = result?.toString() ?: stringResource(when(mediaRequest) {
            MediaManager.MediaRequest.Photo -> R.string.add_picture
            MediaManager.MediaRequest.Video -> R.string.add_video
            MediaManager.MediaRequest.Sound -> R.string.add_voice_recording
        }),
        style = themeTypography.body2,
        color = result?.let { Color.Gray } ?: MaterialTheme.colors.onSurface,
    )
}

private fun provideTopBarResources(): ToolbarResources? {
    val backNavigationType = BackNavigationType.Close
    val titleSmallRes = R.string.add_entry
    val actionRes = R.string.add
    return ToolbarResources(
        titleSmallRes = titleSmallRes,
        backNavigationType = backNavigationType,
        actionRes = actionRes
    )
}

private fun CoroutineScope.request(
    activity: BaseActivity,
    mediaRequest: MediaManager.MediaRequest
) = launch {
    if (mediaRequest != MediaManager.MediaRequest.Sound) {
        val permissionsToRequest = Manifest.permission.CAMERA
        val result = activity.permissionsBuilder(permissionsToRequest).build().sendSuspend()
        if (result.first().isGranted()) {
            when (mediaRequest) {
                MediaManager.MediaRequest.Photo ->
                    ExternalResultsManager.uriExternalResultsRequest.emit(TakePicture)
                MediaManager.MediaRequest.Video ->
                    ExternalResultsManager.uriExternalResultsRequest.emit(TakeVideo)
            }
        }
    } else {
        ExternalResultsManager.uriExternalResultsRequest.emit(RecordSound)
    }
}

private val screenPadding = 16.dp
private val bottomItemPadding = 16.dp
private val commonModifier = Modifier.padding(bottom = bottomItemPadding).fillMaxWidth()