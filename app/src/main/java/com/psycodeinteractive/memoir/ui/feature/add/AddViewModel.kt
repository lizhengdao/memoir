package com.psycodeinteractive.memoir.ui.feature.add

import androidx.lifecycle.viewModelScope
import com.psycodeinteractive.memoir.base.ui.BaseViewModel
import com.psycodeinteractive.memoir.data.model.Entry
import com.psycodeinteractive.memoir.data.repository.EntryRepository
import com.psycodeinteractive.memoir.util.date.getCurrentDateTime
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onCompletion

class AddViewModel(
    private val entryRepository: EntryRepository
): BaseViewModel<AddState, AddEvent>() {

    override fun getInitialState() = AddState()

    fun saveEntry() {
        state.current.run {
            val entry = Entry(
                text = textNote,
                timeStamp = getCurrentDateTime(),
                pictureUrl = pictureUri,
                videoUrl = videoUri,
                voiceRecUrl = soundUri
            )
            entryRepository.add(entry)
                .onCompletion { events.emit(AddEvent.EntrySaved) }
                .launchIn(viewModelScope)
        }
    }

    fun onTextNoteChanged(text: String) {
        state.mutate { textNote = text }
    }

    fun onPictureTaken(uri: String) {
        state.mutate { pictureUri = uri }
    }

    fun onVoiceRecorded(uri: String) {
        state.mutate { soundUri = uri }
    }

    fun onVideoTaken(uri: String) {
        state.mutate { videoUri = uri }
    }
}