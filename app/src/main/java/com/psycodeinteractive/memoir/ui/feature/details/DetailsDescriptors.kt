package com.psycodeinteractive.memoir.ui.feature.details

import com.psycodeinteractive.memoir.base.ui.CommonState
import com.psycodeinteractive.memoir.base.ui.Event
import com.psycodeinteractive.memoir.data.model.Entry

data class DetailsState(
    var entry: Entry? = null
): CommonState()

enum class DetailsEvent: Event {
    EntryRemoved
}
