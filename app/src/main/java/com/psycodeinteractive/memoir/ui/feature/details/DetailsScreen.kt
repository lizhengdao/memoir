package com.psycodeinteractive.memoir.ui.feature.details

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.onActive
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ContextAmbient
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.psycodeinteractive.memoir.R
import com.psycodeinteractive.memoir.base.ui.MemoirScreen
import com.psycodeinteractive.memoir.ui.widget.BackNavigationType
import com.psycodeinteractive.memoir.ui.widget.GlideImage
import com.psycodeinteractive.memoir.ui.widget.MemoirTopBar
import com.psycodeinteractive.memoir.ui.widget.ToolbarResources
import com.psycodeinteractive.memoir.util.date.formatDate
import com.psycodeinteractive.memoir.util.ui.collectEventsAsComposeState
import com.psycodeinteractive.memoir.util.ui.collectStateAsComposeState
import com.psycodeinteractive.memoir.util.ui.themeTypography

@Composable
fun DetailsScreen(
    entryId: Long,
    exitScreen: () -> Unit
) {
    MemoirScreen<DetailsViewModel> { viewModel ->
        onActive {
            viewModel.setup(entryId)
        }
        Column {
            MemoirTopBar(
                resources = provideTopBarResources(),
                onActionClick = { viewModel.removeEntry() }
            ) {
                exitScreen()
            }
            Column(
                modifier = Modifier.padding(contentPadding)
            ) {
                val state = viewModel.collectStateAsComposeState()
                Text(
                    modifier = Modifier.fillMaxWidth()
                        .align(Alignment.Start)
                        .padding(bottom = itemBottomPadding),
                    text = state.entry?.timeStamp.formatDate(),
                    color = Color.LightGray,
                    style = themeTypography.caption
                )
                Text(
                    modifier = Modifier.fillMaxWidth()
                        .align(Alignment.Start),
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis,
                    text = state.entry?.text.orEmpty(),
                    style = themeTypography.body1
                )
                state.entry?.pictureUrl?.let { GlideImage(model = it) }
            }
        }

        when (viewModel.collectEventsAsComposeState()) {
            DetailsEvent.EntryRemoved -> {
                Toast.makeText(
                    ContextAmbient.current,
                    stringResource(id = R.string.entry_removed),
                    Toast.LENGTH_LONG
                ).show()
                exitScreen()
            }
        }
    }
}

private fun provideTopBarResources(): ToolbarResources? {
    val backNavigationType = BackNavigationType.Close
    val titleSmallRes = R.string.entry_details
    val actionRes = R.string.remove_entry
    return ToolbarResources(
        titleSmallRes = titleSmallRes,
        backNavigationType = backNavigationType,
        actionRes = actionRes
    )
}

private val contentPadding = 16.dp
private val itemBottomPadding = 8.dp