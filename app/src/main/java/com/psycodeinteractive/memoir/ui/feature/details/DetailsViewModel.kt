package com.psycodeinteractive.memoir.ui.feature.details

import androidx.lifecycle.viewModelScope
import com.psycodeinteractive.memoir.base.ui.BaseViewModel
import com.psycodeinteractive.memoir.data.repository.EntryRepository
import com.psycodeinteractive.memoir.data.specification.entry.EntryByIdSpecs
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onEmpty

class DetailsViewModel(
    private val entryRepository: EntryRepository
): BaseViewModel<DetailsState, DetailsEvent>() {
    
    fun setup(entryId: Long) = retrieveEntryById(entryId)
    
    private fun retrieveEntryById(entryId: Long) {
        val specs = EntryByIdSpecs(entryId)
        entryRepository.querySingle(specs)
            .onEach {
                state.mutate {
                    entry = it
                }
            }
            .launchIn(viewModelScope)
    }

    fun removeEntry() {
        val specs = EntryByIdSpecs(state.current.entry!!.id)
        entryRepository.remove(specs)
            .onEmpty {
                DetailsEvent.EntryRemoved.dispatch()
            }
            .launchIn(viewModelScope)
    }

    override fun getInitialState() = DetailsState()
}