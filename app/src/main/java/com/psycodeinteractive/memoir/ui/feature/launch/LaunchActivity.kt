package com.psycodeinteractive.memoir.ui.feature.launch

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.psycodeinteractive.memoir.base.ui.BaseActivity
import com.psycodeinteractive.memoir.ui.feature.add.AddScreen
import com.psycodeinteractive.memoir.ui.feature.details.DetailsScreen
import com.psycodeinteractive.memoir.ui.feature.list.ListScreen
import com.psycodeinteractive.memoir.ui.feature.splash.SplashScreen
import com.psycodeinteractive.memoir.util.ui.asName
import com.psycodeinteractive.memoir.util.ui.destination
import com.psycodeinteractive.memoir.util.ui.getArg
import com.psycodeinteractive.memoir.util.ui.navigateTo

private val initialRoute = LaunchRoute.Splash

class LaunchActivity: BaseActivity() {

    @Composable
    override fun ViewRoot() = Navigation()
}

@Composable
private fun Navigation() {
    val navController = rememberNavController()

    NavHost(
        startDestination = initialRoute.asName,
        navController = navController
    ) {
        destination(LaunchRoute.Splash) {
            SplashScreen {
                navController.popBackStack()
                navController.navigateTo(LaunchRoute.List)
            }
        }
        destination(LaunchRoute.List) {
            ListScreen(goToAddEntry = {
                navController.navigateTo(LaunchRoute.Add)
            }) { entryId ->
                navController.navigateTo(
                    LaunchRoute.Details, LaunchRoute.Args.EntryId to entryId)
            }
        }
        destination(LaunchRoute.Details) {
            val entryId = it.getArg<String>(LaunchRoute.Args.EntryId)
            DetailsScreen(entryId.toLong()) {
                navController.popBackStack()
            }
        }
        destination(LaunchRoute.Add) {
            AddScreen {
                navController.popBackStack()
            }
        }
    }
}