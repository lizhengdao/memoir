package com.psycodeinteractive.memoir.ui.feature.launch

import com.psycodeinteractive.core.ui.route.Route
import com.psycodeinteractive.core.ui.route.RouteArg
import com.psycodeinteractive.memoir.base.ui.CommonState

enum class LaunchRoute(
    override val args: String? = null
): Route {
    Splash,
    List,
    Details("/{${Args.EntryId}}"),
    Add;

    enum class Args: RouteArg {
        EntryId
    }
}