package com.psycodeinteractive.memoir.ui.feature.list

import com.psycodeinteractive.memoir.base.ui.CommonState
import com.psycodeinteractive.memoir.base.ui.Event
import com.psycodeinteractive.memoir.data.model.Entry
import com.psycodeinteractive.memoir.util.date.getCurrentDateTime
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

data class ListState(
    var currentDay: LocalDate = getCurrentDateTime().date,
    var entries: List<Entry> = listOf()
): CommonState()

enum class ListEvent: Event {
    ListLoadingStarted,
    ListLoadingFinished
}
