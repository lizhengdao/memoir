package com.psycodeinteractive.memoir.ui.feature.list

import androidx.compose.animation.*
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumnFor
import androidx.compose.material.*
import androidx.compose.material.ripple.RippleIndication
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawOpacity
import androidx.compose.ui.gesture.scrollorientationlocking.Orientation
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.psycodeinteractive.memoir.R
import com.psycodeinteractive.memoir.base.ui.MemoirScreen
import com.psycodeinteractive.memoir.data.model.Entry
import com.psycodeinteractive.memoir.ui.widget.BackNavigationType
import com.psycodeinteractive.memoir.ui.widget.MemoirTopBar
import com.psycodeinteractive.memoir.ui.widget.ToolbarResources
import com.psycodeinteractive.memoir.util.date.formatDate
import com.psycodeinteractive.memoir.util.date.isToday
import com.psycodeinteractive.memoir.util.ui.collectEventsAsComposeState
import com.psycodeinteractive.memoir.util.ui.collectStateAsComposeState
import com.psycodeinteractive.memoir.util.ui.themeTypography
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.datetime.datepicker
import kotlinx.datetime.LocalDate
import kotlinx.datetime.toKotlinLocalDate

@Composable
fun ListScreen(
    goToAddEntry: () -> Unit,
    goToEntryDetails: (entryId: Long) -> Unit
) {
    MemoirScreen<ListViewModel> { viewModel ->
        val state = viewModel.collectStateAsComposeState()
        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            val showAction = mutableStateOf(!state.currentDay.isToday())
            MemoirTopBar(
                resources = provideTopBarResources(),
                showAction = showAction.value,
                onActionClick = {
                    viewModel.jumpToToday()
                }
            )
            CurrentDay(
                viewModel = viewModel,
                currentDay = state.currentDay
            )
            Divider(
                color = topDividerColor
            )
            EntriesList(
                entries = state.entries,
                viewModel = viewModel,
                goToAddEntry = goToAddEntry,
                goToEntryDetails = goToEntryDetails
            )
            HandleEvents(
                viewModel = viewModel
            )
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun ColumnScope.EntriesList(
    entries: List<Entry>,
    viewModel: ListViewModel,
    goToAddEntry: () -> Unit,
    goToEntryDetails: (entryId: Long) -> Unit
) {
    Box(
        modifier = Modifier.fillMaxSize()
            .draggable(
                orientation = Orientation.Horizontal,
                onDragStopped = {
                    if (it < 0) {
                        viewModel.setupCurrentDayNext()
                    } else if (it > 0) {
                        viewModel.setupCurrentDayPrevious()
                    }
                }
            ) { }
    ) {
        EmptyListIndicator(
            entries = entries
        )
        this@EntriesList.AnimatedVisibility(
            visible = !entries.isNullOrEmpty(),
            initiallyVisible = false
        ) {
            Box(
                modifier = Modifier.fillMaxSize()
            ) {
                LazyColumnFor(
                    items = entries
                ) { entry ->
                    EntryItem(entry = entry) { entryId ->
                        goToEntryDetails(entryId)
                    }
                }
            }
        }
        ExtendedFloatingActionButton(
            modifier = Modifier.padding(fabPadding)
                .align(Alignment.BottomEnd),
            text = { Text(text = stringResource(id = R.string.add_new_entry)) },
            icon = { Icon(asset = vectorResource(id = R.drawable.ic_add)) },
            onClick = { goToAddEntry() }
        )
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun EmptyListIndicator(
    entries: List<Entry>
) {
    val alpha = animate(if (entries.isNullOrEmpty()) 1f else 0f)
    Box(
        modifier = Modifier.fillMaxSize()
            .drawOpacity(alpha)
    ) {
        Text(
            modifier = Modifier.align(Alignment.Center),
            text = stringResource(R.string.no_entries_for_day),
            textAlign = TextAlign.Center,
            style = themeTypography.h5
        )
    }
}

@Composable
private fun ColumnScope.CurrentDay(
    viewModel: ListViewModel,
    currentDay: LocalDate
) {
    val dialog = createDatePickerDialog(viewModel)
    Text(
        modifier = Modifier.fillMaxWidth()
            .align(Alignment.CenterHorizontally)
            .clickable { dialog.show() }
            .padding(horizontal = datePaddingHorizontal, vertical = datePaddingVertical),
        text = currentDay.formatDate() + " >",
        style = themeTypography.h4
    )
}

@Composable
private fun createDatePickerDialog(viewModel: ListViewModel): MaterialDialog {
    val dialog = MaterialDialog()
    dialog.build {
        datepicker { viewModel.setupCurrentDay(it.toKotlinLocalDate()) }
    }
    return dialog
}

@Composable
private fun HandleEvents(viewModel: ListViewModel) {
    var loading by mutableStateOf(false)
    when (viewModel.collectEventsAsComposeState()) {
        ListEvent.ListLoadingStarted -> loading = true
        ListEvent.ListLoadingFinished -> loading = false
    }
    if (loading) {
        CircularProgressIndicator()
    }
}

@Composable
private fun EntryItem(
    entry: Entry,
    onClick: (entryId: Long) -> Unit
) {
    val ripple = RippleIndication(
        color = MaterialTheme.colors.primary
    )
    Box(
        modifier = Modifier.clickable(indication = ripple) { onClick(entry.id) }
    ) {
        Column(
            modifier = Modifier.fillMaxWidth()
                .wrapContentHeight()
                .padding(start = entryPadding, end = entryPadding, top = entryPadding)
        ) {
            Row(
                modifier = Modifier.align(Alignment.End)
            ) {
                Text(
                    modifier = Modifier.align(Alignment.Top),
                    text = entry.timeStamp.formatDate(),
                    color = Color.LightGray,
                    style = themeTypography.caption
                )
            }
            Row(
                modifier = Modifier.align(Alignment.Start)
            ) {
                Text(
                    modifier = Modifier.fillMaxWidth(0.7f)
                        .align(Alignment.CenterVertically),
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis,
                    text = entry.text.orEmpty(),
                    style = themeTypography.body1
                )
            }
            Divider(
                modifier = Modifier.padding(top = entryPadding),
                color = dividerColor
            )
        }
    }
}


private fun provideTopBarResources(): ToolbarResources? {
    val backNavigationType = BackNavigationType.None
    val titleBigRes = R.string.app_name
    val actionRes = R.string.jump_to_today
    return ToolbarResources(
        titleBigRes = titleBigRes,
        backNavigationType = backNavigationType,
        actionRes = actionRes
    )
}

private val fabPadding = 16.dp
private val entryPadding = 16.dp
private val datePaddingHorizontal = 16.dp
private val datePaddingVertical = 6.dp

private val dividerColor = Color.LightGray.copy(0.3f)
private val topDividerColor = Color.LightGray.copy(0.35f)