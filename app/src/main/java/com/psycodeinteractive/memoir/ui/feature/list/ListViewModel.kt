package com.psycodeinteractive.memoir.ui.feature.list

import androidx.lifecycle.viewModelScope
import com.psycodeinteractive.memoir.base.ui.BaseViewModel
import com.psycodeinteractive.memoir.data.repository.EntryRepository
import com.psycodeinteractive.memoir.data.specification.entry.EntriesByDaySpecs
import com.psycodeinteractive.memoir.util.date.getCurrentDateTime
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.datetime.DatePeriod
import kotlinx.datetime.LocalDate
import kotlinx.datetime.minus
import kotlinx.datetime.plus

class ListViewModel(
    private val entryRepository: EntryRepository
): BaseViewModel<ListState, ListEvent>() {

    init {
        retrieveEntriesForCurrentDate()
    }

    private fun retrieveEntriesForDay(date: LocalDate) {
        val specs = EntriesByDaySpecs(date)
        entryRepository.query(specs)
            .onStart {
                state.mutate { entries = listOf() }
                ListEvent.ListLoadingStarted.dispatch()
            }
            .onEach {
                ListEvent.ListLoadingFinished.dispatch()
                state.mutate {
                    entries = it
                }
            }
            .launchIn(viewModelScope)
    }

    private fun retrieveEntriesForCurrentDate() = retrieveEntriesForDay(state.current.currentDay)

    fun jumpToToday() {
        state.mutate {
            currentDay = getCurrentDateTime().date
        }
        retrieveEntriesForCurrentDate()
    }

    fun setupCurrentDay(date: LocalDate) {
        if (date != state.current.currentDay) {
            state.mutate {
                currentDay = date
            }
            retrieveEntriesForCurrentDate()
        }
    }

    fun setupCurrentDayPrevious() {
        setupCurrentDay(state.current.currentDay + DatePeriod(days = -1))
    }

    fun setupCurrentDayNext() {
        setupCurrentDay(state.current.currentDay + DatePeriod(days = 1))
    }

    override fun getInitialState() = ListState()
}