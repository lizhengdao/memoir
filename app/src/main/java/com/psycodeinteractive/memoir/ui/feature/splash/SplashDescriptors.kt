package com.psycodeinteractive.memoir.ui.feature.splash

import com.psycodeinteractive.memoir.base.ui.CommonState
import com.psycodeinteractive.memoir.base.ui.Event

data class SplashState(
    var splashTimeMs: Long = 2000,
    var splashFinished: Boolean = false
): CommonState()

enum class SplashEvent: Event {
    SplashFinished
}