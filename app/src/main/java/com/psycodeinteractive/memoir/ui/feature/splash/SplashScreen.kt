package com.psycodeinteractive.memoir.ui.feature.splash

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.sp
import com.psycodeinteractive.memoir.R
import com.psycodeinteractive.memoir.base.ui.MemoirScreen
import com.psycodeinteractive.memoir.ui.feature.splash.SplashEvent.SplashFinished
import com.psycodeinteractive.memoir.util.ui.collectEventsAsComposeState

@Composable
fun SplashScreen(
    onSplashFinished: () -> Unit
) {
    MemoirScreen<SplashViewModel> { viewModel ->
        Box(modifier = Modifier.fillMaxSize()
            .wrapContentSize(Alignment.Center),
        ) {
            Text(
                modifier = Modifier.wrapContentSize(),
                text = stringResource(id = R.string.app_name),
                fontSize = 32.sp,
                color = MaterialTheme.colors.primary
            )
        }

        when (viewModel.collectEventsAsComposeState()) {
            SplashFinished -> onSplashFinished()
        }
    }
}