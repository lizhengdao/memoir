package com.psycodeinteractive.memoir.ui.feature.splash

import androidx.lifecycle.viewModelScope
import com.psycodeinteractive.memoir.base.ui.BaseViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashViewModel: BaseViewModel<SplashState, SplashEvent>() {

    init {
        viewModelScope.launch {
            if (!state.current.splashFinished)
                delay(state.current.splashTimeMs)

            state.mutate { splashFinished = true }
            SplashEvent.SplashFinished.dispatch()
        }
    }

    override fun getInitialState() = SplashState()
}