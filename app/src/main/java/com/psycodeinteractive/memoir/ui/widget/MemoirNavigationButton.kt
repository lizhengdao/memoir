package com.psycodeinteractive.memoir.ui.widget

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Row
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.DrawLayerModifier
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.vectorResource
import com.psycodeinteractive.memoir.R

@Composable
fun MemoirNavigationButton(
        modifier: Modifier = Modifier,
        type: BackNavigationType = BackNavigationType.Close,
        onClick: ((BackNavigationType) -> Unit)?
) {
    val navIcon = when (type) {
        BackNavigationType.Close -> R.drawable.icon_close
        BackNavigationType.None -> null
    }

    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        IconButton(onClick = { onClick?.invoke(type) }) {
            navIcon?.run {
                val navIconVector = vectorResource(id = this)
                val layerModifier = object : DrawLayerModifier {
                    override val rotationZ = if (type == BackNavigationType.Close) 45f else 0f
                }
                Image(
                        modifier = layerModifier,
                        asset = navIconVector,
                        contentScale = ContentScale.Fit,
                        colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface)
                )
            }
        }
    }
}