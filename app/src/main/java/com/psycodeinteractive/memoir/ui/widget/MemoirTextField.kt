package com.psycodeinteractive.memoir.ui.widget

import androidx.compose.foundation.clickable
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.ExperimentalFocus
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.isFocused
import androidx.compose.ui.focusObserver
import androidx.compose.ui.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.node.Ref
import androidx.compose.ui.text.SoftwareKeyboardController
import androidx.compose.ui.text.input.TextFieldValue
import com.psycodeinteractive.memoir.util.ui.themeTypography

@OptIn(ExperimentalFocus::class)
@Composable
fun MemoirTextField(
    modifier: Modifier = Modifier,
    value: TextFieldValue = TextFieldValue(),
    placeholder: @Composable (() -> Unit)? = null,
    onValueChanged: (value: TextFieldValue) -> Unit
) {
    var textFieldValue by remember { mutableStateOf(value) }
    val focusRequester = FocusRequester()
    val textFieldModifier = modifier
        .focusRequester(focusRequester)
        .clickable {
            focusRequester.requestFocus()
        }
    OutlinedTextField(
        modifier = textFieldModifier then modifier,
        value = textFieldValue,
        textStyle = themeTypography.body2.copy(color = MaterialTheme.colors.onSurface),
        onValueChange = {
            textFieldValue = it
            onValueChanged(it)
        },
        placeholder = placeholder
    )
}