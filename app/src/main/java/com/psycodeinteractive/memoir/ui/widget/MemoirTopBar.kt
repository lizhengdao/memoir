package com.psycodeinteractive.memoir.ui.widget

import androidx.annotation.StringRes
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.psycodeinteractive.memoir.util.ui.semiBold
import com.psycodeinteractive.memoir.util.ui.themeTypography

enum class BackNavigationType {
    Close, None
}

data class ToolbarResources(
    @StringRes val titleBigRes: Int? = null,
    @StringRes val titleSmallRes: Int? = null,
    @StringRes val actionRes: Int? = null,
    val backNavigationType: BackNavigationType = BackNavigationType.None
)

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun MemoirTopBar(
    modifier: Modifier = Modifier,
    resources: ToolbarResources?,
    showAction: Boolean = true,
    onActionClick: (() -> Unit)? = null,
    onClick: ((BackNavigationType) -> Unit)? = null
) {
    if (resources == null || (resources.titleBigRes == null
                && resources.titleSmallRes == null)) {
        return
    }

    val titleBig = resources.titleBigRes?.let { stringResource(it) }
    val titleSmall = resources.titleSmallRes?.let { stringResource(it) }
    val actionsString = resources.actionRes?.let { stringResource(it) }

    val isTitleBigProvided = titleBig != null
    val isBackNavProvided = resources.backNavigationType != BackNavigationType.None

    val titleBigPadding = if (isTitleBigProvided) 24.dp else 0.dp

    val paddingModifier = Modifier.absolutePadding(
        top = titleBigPadding,
        bottom = titleBigPadding,
        left = 0.dp,
        right = 0.dp
    )

    TopAppBar(modifier = modifier
            then Modifier.background(MaterialTheme.colors.surface)
            then paddingModifier,
        backgroundColor = Color.Transparent,
        elevation = 0.dp
    ) {
        Box(
            modifier = Modifier.wrapContentHeight().fillMaxWidth()
        ) {
            MemoirNavigationButton(
                modifier = Modifier.align(Alignment.CenterStart)
                    .let {
                        if (isBackNavProvided) {
                            it.wrapContentSize()
                        } else
                            it.size(0.dp)
                    },
                type = resources.backNavigationType,
                onClick = onClick
            )
            val textAlignment = titleBig?.let { Alignment.CenterStart }
                ?: titleSmall?.let { Alignment.Center }!!
            Text(
                modifier = Modifier.padding(12.dp)
                    .align(textAlignment),
                text = titleBig ?: titleSmall!!,
                color = MaterialTheme.colors.onSurface,
                style = titleBig?.let { themeTypography.h5 }
                    ?: titleSmall?.let { themeTypography.subtitle1.semiBold() }!!
            )

            actionsString?.run {
                this@TopAppBar.AnimatedVisibility(
                    modifier = Modifier.wrapContentSize()
                        .align(Alignment.CenterEnd),
                    visible = showAction
                ) {
                    Text(
                        modifier = Modifier.padding(end = 16.dp)
                            .clickable {
                                onActionClick?.invoke()
                            },
                        text = this,
                        maxLines = 1,
                        color = MaterialTheme.colors.primary,
                        style = themeTypography.subtitle1
                    )
                }
            }
        }
    }
}