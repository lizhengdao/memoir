@file:Suppress("UNCHECKED_CAST")

package com.psycodeinteractive.memoir.util

import android.content.ActivityNotFoundException
import android.net.Uri
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.result.ActivityResultLauncher
import androidx.lifecycle.lifecycleScope
import com.psycodeinteractive.memoir.R
import com.psycodeinteractive.memoir.base.ui.BaseActivity
import com.psycodeinteractive.memoir.util.media.MediaManager
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

object ExternalResultsManager {

    val uriExternalResults = MutableSharedFlow<Pair<ResultContract, Uri>>()
    val uriExternalResultsRequest = MutableSharedFlow<ResultContract>()

    lateinit var soundRecordRequest: ActivityResultLauncher<Unit?>
    lateinit var takePictureRequest: ActivityResultLauncher<Uri?>
    lateinit var takeVideoRequest: ActivityResultLauncher<Uri?>

    lateinit var photoUri: Uri
    lateinit var videoUri: Uri

    fun init(
        activity: BaseActivity,
    ) {
        soundRecordRequest = MediaManager.requestAudioRecording(activity) {
            activity.emitResult(RecordSound, it)
        } as ActivityResultLauncher<Unit?>
        takePictureRequest = MediaManager.requestTakePicture(activity) {
            activity.emitResult(TakePicture, it)
        } as ActivityResultLauncher<Uri?>
        takeVideoRequest = MediaManager.requestTakeVideo(activity) {
            activity.emitResult(TakeVideo, it)
        } as ActivityResultLauncher<Uri?>

        photoUri = MediaManager.getNewMediaUri(activity)!!
        videoUri = MediaManager.getNewMediaUri(activity)!!

        activity.lifecycleScope.launch {
            uriExternalResultsRequest.collect {
                try {
                    when (it) {
                        RecordSound -> soundRecordRequest.launch(null)
                        TakePicture -> takePictureRequest.launch(photoUri)
                        TakeVideo -> takeVideoRequest.launch(videoUri)
                    }
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(
                            activity,
                            activity.getString(R.string.no_application_can_handle),
                            Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    private fun ComponentActivity.emitResult(contract: ResultContract, uri: Uri?) {
        uri?.run {
            lifecycleScope.launch {
                uriExternalResults.emit(contract to uri)
            }
        }
    }
}