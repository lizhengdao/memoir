package com.psycodeinteractive.memoir.util

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import androidx.activity.result.contract.ActivityResultContract

typealias ResultContract = ActivityResultContract<Any, Any>

object RecordSound : ResultContract() {
    override fun parseResult(resultCode: Int, result: Intent?) : Uri? {
        if (resultCode != RESULT_OK) {
            return null
        }
        return result?.data
    }

    override fun createIntent(context: Context, input: Any?)
            = Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION)
}

object TakePicture : ResultContract() {
    override fun createIntent(context: Context, input: Any?) =
        Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            .putExtra(MediaStore.EXTRA_OUTPUT, input as Uri?)

    override fun parseResult(resultCode: Int, result: Intent?): Uri? {
        if (resultCode != RESULT_OK) {
            return null
        }
        return ExternalResultsManager.photoUri
    }
}

object TakeVideo : ResultContract() {
    override fun createIntent(context: Context, input: Any?) =
        Intent(MediaStore.ACTION_VIDEO_CAPTURE)
            .putExtra(MediaStore.EXTRA_OUTPUT, input as Uri?)

    override fun parseResult(resultCode: Int, result: Intent?) : Uri? {
        if (resultCode != RESULT_OK) {
            return null
        }
        return ExternalResultsManager.videoUri
    }
}