package com.psycodeinteractive.memoir.util.date

import android.text.format.DateUtils
import androidx.compose.runtime.Composable
import kotlinx.datetime.*
import java.time.format.DateTimeFormatter

@Composable
fun LocalDateTime?.formatDate(): String {
    return this?.run {
        DateTimeFormatter.RFC_1123_DATE_TIME.format(
            toJavaLocalDateTime().atZone(TimeZone.UTC.toJavaZoneId())
        ).orEmpty()
    }.orEmpty()
}

@Composable
fun LocalDate?.formatDate(): String {
    return this?.run {
        DateTimeFormatter.ISO_LOCAL_DATE.format(
            toJavaLocalDate()
        ).orEmpty()
    }.orEmpty()
}

fun getCurrentDateTime() = Clock.System.now().toLocalDateTime(TimeZone.UTC)

@Composable
fun LocalDate.isToday() = compareTo(getCurrentDateTime().date) == 0