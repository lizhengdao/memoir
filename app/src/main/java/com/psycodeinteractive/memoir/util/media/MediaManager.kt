@file:Suppress("UNCHECKED_CAST")

package com.psycodeinteractive.memoir.util.media

import android.content.ContentValues
import android.net.Uri
import android.provider.MediaStore
import com.psycodeinteractive.memoir.base.ui.BaseActivity
import com.psycodeinteractive.memoir.util.RecordSound
import com.psycodeinteractive.memoir.util.TakePicture
import com.psycodeinteractive.memoir.util.TakeVideo


object MediaManager {

    fun requestAudioRecording(activity: BaseActivity, result: (Uri?) -> Unit) =
        activity.registerForActivityResult(RecordSound, result as ((Any?) -> Unit))

    fun requestTakePicture(activity: BaseActivity, result: (Uri?) -> Unit) =
        activity.registerForActivityResult(TakePicture, result as ((Any?) -> Unit))

    fun requestTakeVideo(activity: BaseActivity, result: (Uri?) -> Unit) =
        activity.registerForActivityResult(TakeVideo, result as ((Any?) -> Unit))

    fun getNewMediaUri(activity: BaseActivity) = activity.contentResolver.insert(
        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
        ContentValues()
    )

    enum class MediaRequest {
        Photo, Video, Sound
    }


}