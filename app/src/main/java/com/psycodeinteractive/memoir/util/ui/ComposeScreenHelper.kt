package com.psycodeinteractive.memoir.util.ui

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.KEY_ROUTE
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigate
import com.psycodeinteractive.core.ui.route.Route
import com.psycodeinteractive.core.ui.route.RouteArg
import com.psycodeinteractive.memoir.base.ui.BaseViewModel
import com.psycodeinteractive.memoir.base.ui.CommonState
import com.psycodeinteractive.memoir.base.ui.Event

@Composable
inline fun <VM : BaseViewModel<T, out Event>, reified T : CommonState>
        VM.collectStateAsComposeState(
): T {
    val composeState = remember { mutableStateOf(getInitialState() ) }
    val s = state.collectAsState(BaseViewModel.StateWrapper(getInitialState()))
    composeState.value = s.value.state
    return composeState.value
}

fun NavController.navigateTo(route: Route, vararg args: Pair<Enum<*>, Any>? = arrayOf()) {
    var routeWithArgs: String = route.asName
    args.takeIf { it.isNotEmpty() }?.forEach {
        val key = "{${it!!.first.name}}"
        val value = it.second.toString()
        routeWithArgs = routeWithArgs.replace(key, value)
    }
    navigate(routeWithArgs)
}

val Route.asName: String
    get() = (this as Enum<*>).name + args

fun NavGraphBuilder.destination(
    route: Route,
    content: @Composable (NavBackStackEntry) -> Unit
) {
    composable(route.asName) {
        content(it)
    }
}

@Composable
fun NavBackStackEntry.currentRouteAsString(): String {
    return arguments?.getString(KEY_ROUTE)!!
}

@Composable
inline fun <reified Data> NavBackStackEntry.getArg(routeArg: RouteArg): Data {
    return arguments!!.get((routeArg as Enum<*>).name) as Data
}

@Composable
inline fun <VM : BaseViewModel<out CommonState, T>, reified T : Event>
        VM.collectEventsAsComposeState(): T? {
    val event = events.collectAsState(null)
    return event.value
}

@Composable
inline fun <reified R: Route> NavBackStackEntry.getCurrentRoute(): R {
    val current = currentRouteAsString()
    val values = R::class.java.enumConstants as Array<Route>
    return values.first { it.asName == current } as R
}