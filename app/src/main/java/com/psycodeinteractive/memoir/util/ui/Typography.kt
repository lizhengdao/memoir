package com.psycodeinteractive.memoir.util.ui

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.font.font
import androidx.compose.ui.text.font.fontFamily
import androidx.compose.ui.unit.sp
import com.psycodeinteractive.memoir.R

val regular = font(R.font.poppins_regular, FontWeight.Normal)
val medium = font(R.font.poppins_medium, FontWeight.Medium)
val semibold = font(R.font.poppins_semibold, FontWeight.SemiBold)
val bold = font(R.font.poppins_bold, FontWeight.Bold)

val appFontFamily = fontFamily(fonts = listOf(regular, medium, semibold, bold))

private val textStyleSemiBoldMap = mutableMapOf<TextStyle, TextStyle>()
private val textStyleBoldMap = mutableMapOf<TextStyle, TextStyle>()
val themeTypography = Typography(
    h4 = TextStyle(
        fontFamily = appFontFamily,
        fontSize = 30.sp
    ),
    h5 = TextStyle(
        fontFamily = appFontFamily,
        fontSize = 24.sp
    ),
    h6 = TextStyle(
        fontFamily = appFontFamily,
        fontSize = 18.sp
    ),
    subtitle1 = TextStyle(
        fontFamily = appFontFamily,
        fontSize = 16.sp
    ),
    subtitle2 = TextStyle(
        fontFamily = appFontFamily,
        fontSize = 12.sp
    ),
    body1 = TextStyle(
        fontFamily = appFontFamily,
        fontSize = 16.sp
    ),
    body2 = TextStyle(
        fontFamily = appFontFamily,
        fontSize = 15.sp
    ),
    button = TextStyle(
        fontFamily = appFontFamily,
        fontSize = 14.sp
    ),
    caption = TextStyle(
        fontFamily = appFontFamily,
        fontSize = 9.5.sp
    ),
    overline = TextStyle(
        fontFamily = appFontFamily,
        fontSize = 12.sp)
)

fun TextStyle.semiBold() = textStyleSemiBoldMap[this] ?: copy(fontWeight = FontWeight.SemiBold).let {
    textStyleSemiBoldMap[this] = it
    return@let it
}

fun TextStyle.bold() = textStyleBoldMap[this] ?: copy(fontWeight = FontWeight.Bold).let {
    textStyleBoldMap[this] = it
    return@let it
}